__author__ = 'pav'

import zmq
import logging

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


def server_forever():
    context = zmq.Context()

    pull_socket = context.socket(getattr(zmq, 'PULL'))
    pull_socket.bind('tcp://127.0.0.1:5000')

    push_socket = context.socket(getattr(zmq, 'PUSH'))
    push_socket.bind('tcp://127.0.0.1:5001')

    while True:
        try:
            task_data = pull_socket.recv_json()
            logger.debug("Send task to worker")
            push_socket.send_json(task_data)

        except Exception, e:
            logger.warn(e.message)


    pull_socket.close()
    push_socket.close()
    context.term()


if __name__ == '__main__':
    logger.info("Starting ZeroMQ master server process.")
    server_forever()