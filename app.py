__author__ = 'pav'
from gevent import monkey
monkey.patch_all()

import bottle
import functools
import zmq
import logging

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

logger = logging.getLogger(__name__)




def cors_enable_hook():
    bottle.response.headers['Access-Control-Allow-Origin'] = '*'
    bottle.response.headers['Access-Control-Allow-Headers'] = \
        'Authorization, Credentials, X-Requested-With, Content-Type'
    bottle.response.headers['Access-Control-Allow-Methods'] = \
        'GET, PUT, POST, OPTIONS, DELETE'


ctx = zmq.Context()
task_socket = ctx.socket(zmq.PUSH)
task_socket.connect('tcp://127.0.0.1:5000')


def user_validator(keys=()):

    def _decorator(func):

        @functools.wraps(func)
        def __decorator(*args, **kwargs):

            if not set(bottle.request.json.keys()).issubset(set(keys)):
                return {"error": "invalid data entry"}
            return func(*args, **kwargs)
        
        return __decorator

    return _decorator


# @user_validator(keys=('user', 'image_id', 'comments'))
def user_review():
    try:
        data = {"log_data": bottle.request.json}
    except TypeError, e:
        print e.message
        return {"error": "invalid json"}

    logger.warn("Request send into queue.")
    task_socket.send_json({ 'task': 'update_like', 'task_kwargs': data})

    return {"status": "updated"}



if __name__ == '__main__':

    bottle.route('/', method=['GET'])(lambda: "Async Update Web Service.")
    bottle.route('/review', method=['POST'])(user_review)
    bottle.route('/review', method=['OPTIONS'])(lambda: '')

    bottle.hook('after_request')(cors_enable_hook)

    bottle.run(server='gevent', host='0.0.0.0', port=6061, reloader=True)