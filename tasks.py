__author__ = 'pav'


import json
from pg_utils import DBPool


persistence = DBPool(
    pool_size=20,
    pool_type="threaded",
    debug=True,
    host="localhost",
    port=5432,
    user="pav",
    password="iverson",
    database="logdb"
)


def update_like(log_data=None):
    """Update user entity_data with validation.
    """

    persistence.query("""SELECT log.log_update($${d}$$)""".format(d=json.dumps(log_data)))

    print "Tasks done: saved"