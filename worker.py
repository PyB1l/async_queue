__author__ = 'pav'

import zmq
import tasks
import logging

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


def server_forever():
    context = zmq.Context()

    socket = context.socket(getattr(zmq, 'PULL'))
    socket.connect('tcp://127.0.0.1:5001')

    while 1:
        try:
            task_data = socket.recv_json()
            task = task_data.pop('task')
            task_kwargs = task_data.pop('task_kwargs')
            getattr(tasks, task)(**task_kwargs)
            logger.debug("Database update task completed")

        except Exception, e:
            logger.warn(e.message)

    socket.close()
    context.term()


if __name__ == "__main__":
    logger.info("Starting ZeroMQ worker server process.")
    server_forever()